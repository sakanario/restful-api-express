// Import packages
const express = require('express');
const morgan = require('morgan');
const app = express();
const PORT = 1337;

// const app = express()
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(require('./routes/index.routes'));

// Morgan
// app.use(morgan('tiny'))

// Starting server
app.listen(PORT,() => {
    console.log(`App run on ${PORT}`);
});

// First route
app.get('/', (req, res) => {
    res.json({ message: 'Hello world' })
});


