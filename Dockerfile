FROM node:latest

ENV NODE_ENV=production

RUN mkdir -p /usr/src/app

# Create app directory
WORKDIR /usr/src/app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . /usr/src/app

EXPOSE 1337

CMD [ "npm", "start" ]