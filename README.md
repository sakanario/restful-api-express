# Restful API with Express js

This app is self project to learn restful api with express js.
This app can do CRUD an article on json file.

## Installation

```bash
npm run dev
```

The app will run on http://localhost:1337

## Usage
```bash
#GetAll  
http://localhost:1337/api/v1/posts/ with get method

# GetbyID : 
http://localhost:1337/api/v1/posts/3 with get method

# Create new Article : 
http://localhost:1337/api/v1/posts with post method

# Delete : 
http://localhost:1337/api/v1/posts/3 with delete method
```

